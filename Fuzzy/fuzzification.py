
class Fuzzy:

    def __init__(self):
        self.tempo_de_emprego = None
        self.tempo_de_pagamento_moradia = None
        self.historico_de_pagamento = None
        self.historico_de_credito = None
        self.fluxo_de_caixa = None
        self.liberacao_de_credito = None

    def setup(self):
        resposta1 = input("Quanto você deeja pedir de crédito?\n")
        print("\n")

        self.tempo_de_emprego = input("Quanto tempo você está empregado?\n")
        print("\n")

        resposta2 = input("Você mora de aluguel ou possui casa financiada?\n")
        print("\n")

        if resposta2 is "s":
            self.tempo_de_pagamento_moradia = input("A quanto tempo voce mora de aluguel ou falta quanto tempo pra "
                                                    "pagar o financiamento?\n")

        resposta3 = input("Você está em débito com a empresa?\n")

        if resposta3 is "s":
            self.historico_de_pagamento = input("Quanto tempo de atraso no pagamento?\n")

        self.historico_de_credito = input("A quanto tempo você é cliente da empresa?\n")

        self.fluxo_de_caixa = input("Quantos porcentos do seu salário você gasta por mês?\n")

        print("")
