import sys

import numpy as np
from Kohonen import SOMNeuron as somn
from Functions import functions as fun
from scipy.spatial import distance
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.colors import ListedColormap, LinearSegmentedColormap


class Kohonen:

    def __init__(self):
        self.map_qtd_neurons = None
        self.map_qtd_dimensions = None
        self.neurons = None
        self.training_entrys = None
        self.test_entrys = None
        self.entry_represention = None
        self.max_epochs = 10000
        self.precision = 0.0001
        self.learning_rate = 41
        self.standart_deviation = 0
        self.neuron_radius = 0
        self.alfa = 0

    def setup(self, map_qtd_neurons, map_qtd_dimensions, precision, learning_rate, standart_deviation, neuron_radius,
              training_entrys, test_entrys, entry_represention, alfa):

        # inicializa a matriz ne neurônios
        if map_qtd_dimensions == 1:
            self.neurons = np.empty([1, map_qtd_neurons], somn.SOMNeuron)
        elif map_qtd_dimensions == 2:
            self.neurons = np.empty([int(np.sqrt(map_qtd_neurons)), int(np.sqrt(map_qtd_neurons))], somn.SOMNeuron)
        else:
            raise Exception("número de dimensões invalido!")

        self.map_qtd_neurons = map_qtd_neurons
        self.map_qtd_dimensions = map_qtd_dimensions

        # configura os parâmetros de treinamento
        self.precision = precision
        self.learning_rate = learning_rate
        self.neuron_radius = neuron_radius
        self.standart_deviation = standart_deviation
        self.alfa = alfa

        # salva a lista de entradas de treino e teste
        self.training_entrys = training_entrys
        self.test_entrys = test_entrys
        self.entry_represention = entry_represention

        # inicializa os neurônios
        for x in range(self.neurons.shape[0]):
            for y in range(self.neurons.shape[1]):
                self.neurons[x][y] = somn.SOMNeuron(training_entrys.shape[1], self.neuron_radius, x, y)

        # define os vizinhos de cada neurônio
        if self.neuron_radius >= 1:
            for x1 in range(self.neurons.shape[0]):
                for y1 in range(self.neurons.shape[1]):
                    for x2 in range(self.neurons.shape[0]):
                        for y2 in range(self.neurons.shape[1]):
                            if 0 < fun.calc_dist_two_points(x1, y1, x2, y2) <= self.neuron_radius:
                                self.neurons[x1][y1].add_neighbor(self.neurons[x2][y2])

    def execute(self):

        epochs = 1
        stop = False
        current_weights = self.get_weights()

        while epochs <= self.max_epochs and not stop:
            for line in range(self.training_entrys.shape[0]):
                winner_neuron = self.calc_winner_neuron(self.training_entrys[line])
                winner_neuron.update_weights(self.training_entrys[line], self.learning_rate, self.standart_deviation)
            epochs += 1

            if self.alfa != 0:
                self.learning_rate = self.alfa * self.learning_rate

            new_weights = self.get_weights()
            stop = self.check_update_enough_to_stop(current_weights, new_weights)
            current_weights = new_weights

            # print(">>>>>>>>>>>>>>>>>>>>>>>>>> epoch = " + str(epochs))
            # TODO: calcular o criterio de parada de comparacao de mudanca significativa

        print(">>>>>>>>>>>>>>>>>>>>>>>>>> epoch = " + str(epochs - 1))
        self.test()

    def test(self):

        class_neurons = None

        if self.map_qtd_dimensions == 1:
            class_neurons = np.empty([1, self.map_qtd_neurons], object)
        elif self.map_qtd_dimensions == 2:
            class_neurons = np.empty([int(np.sqrt(self.map_qtd_neurons)), int(np.sqrt(self.map_qtd_neurons))], object)

        for line in range(self.test_entrys.shape[0]):
            winner_neuron = self.calc_winner_neuron(self.test_entrys[line])
            try:
                class_neurons[winner_neuron.x][winner_neuron.y] += "(" + self.entry_represention[line] + ")"
            except:
                class_neurons[winner_neuron.x][winner_neuron.y] = "(" + self.entry_represention[line] + ")"

        table = ""
        for x in range(class_neurons.shape[0]):
            for y in range(class_neurons.shape[1]):
                table += str(class_neurons[x][y]) + "\t\t\t"
            table += "\n"

        print(table)
        # self.print_matrix()

    def print_matrix(self):

        fig, ax = plt.subplots()

        weight_matrix = np.empty(self.neurons.shape)

        for x in range(self.neurons.shape[0]):
            for y in range(self.neurons.shape[1]):
                weight_matrix[x][y] = np.sum(abs(self.neurons[x][y].weights))
                ax.text(x + 0.5, y + 0.5, str(weight_matrix[x][y]), va='center', ha='center')

        plt.matshow(weight_matrix, cmap=plt.cm.Blues)

        min_val = 0
        max_val = self.neurons.shape[1]

        ax.set_xlim(0, self.neurons.shape[1])
        ax.set_ylim(0, self.neurons.shape[1])
        ax.set_xticks(np.arange(max_val))
        ax.set_yticks(np.arange(max_val))
        ax.grid()

        plt.show()

    def calc_winner_neuron(self, x):
        """
        Calcula a distância euclidiana entre training_entrys(k) e o neurônio(j) e retorna como vencedor o neurônio que
        obtiver a menor distância euclidiana.

        Parameters
        ----------
        x : a linha da entrada que está sendo treinada

        :return: SOMNeuron
        """

        winner_neuron = self.neurons[0][0]
        winner_distance = 10000

        for line in range(self.neurons.shape[0]):
            for column in range(self.neurons.shape[1]):
                winner_test_distance = distance.euclidean(x, self.neurons[line][column].weights)
                if winner_test_distance < winner_distance:
                    winner_neuron = self.neurons[line][column]
                    winner_distance = winner_test_distance

        return winner_neuron

    def get_weights(self):
        weights = np.empty([self.map_qtd_neurons, self.training_entrys.shape[1]], float)
        w_index = 0

        for line in range(self.neurons.shape[0]):
            for column in range(self.neurons.shape[1]):
                weights[w_index] = self.neurons[line][column].weights.copy()
                w_index += 1

        return weights

    def check_update_enough_to_stop(self, old_weights, new_weights):
        diference_enough = abs(new_weights - old_weights) > self.precision

        for line in range(diference_enough.shape[0]):
            for column in range(diference_enough.shape[1]):
                if diference_enough[line][column]:
                    return False

        return True
