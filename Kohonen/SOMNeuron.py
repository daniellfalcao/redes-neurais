import numpy as np
from Functions import functions as fun


class SOMNeuron:

    def __init__(self, qtd_weights, neighbors_radius, cx, cy):
        self.weights = np.random.uniform(-1, 1, qtd_weights)
        self.neighbors_radius = neighbors_radius
        self.neighbors = []
        self.x = cx
        self.y = cy

    def add_neighbor(self, neuron):
        self.neighbors.append(neuron)

    def update_weights(self, x, learning_rate, standart_deviation):
        """
        Atualiza os pesos do neurônio e do seus vizinhos.

        Parameters
        ----------
        x : é o padrão utilizado no cálculo da atualização dos pesos.
        learning_rate: é a taxa de aprendizado
        standart_deviation: é o desvio padrão da função gaussiana
        """

        self.weights += learning_rate * (x - self.weights)

        for index in range(len(self.neighbors)):
            if self.neighbors_radius == 1:
                self.neighbors[index].weights += (learning_rate / 2) * (x - self.neighbors[index].weights)
            else:
                x1 = self.x
                x2 = self.neighbors[index].x
                y1 = self.y
                y2 = self.neighbors[index].y
                a = np.exp(-((fun.calc_dist_two_points(x1, y1, x2, y2)) / (2 * (standart_deviation ** 2))))

                self.neighbors[index].weights += (learning_rate * a) * (x - self.neighbors[index].weights)
