import re
import numpy as np


class DatasetReader:

    @staticmethod
    def __convert_data(row):
        return re.sub('\t', ' ', row).strip().split(" ")

    @staticmethod
    def get_dataset(dataset_list):
        """ Função para a leitura dos dados

        Returns:matriz
            [][]: matriz do Dataset
        """

        ### inicializa a matriz vazia
        matrix = [[]]

        ### adiciona cada linha do Dataset a matriz
        for line in dataset_list:
            try:
                matrix = np.vstack((matrix, [DatasetReader.__convert_data(line)]))
            except:
                matrix = np.array([DatasetReader.__convert_data(dataset_list[0])])

        return matrix.astype(float)

    @staticmethod
    def get_dataset_for_class(dataset_list_x, dataset_list_d, class1, class2):

        dataset_x = DatasetReader.get_dataset(dataset_list_x)
        dataset_d = DatasetReader.get_dataset(dataset_list_d)

        dataset = np.concatenate((dataset_x, dataset_d), axis=1)

        dataset_classe1 = dataset[dataset[:, dataset.shape[1] - 1] == class1]
        dataset_classe1[:, dataset_classe1.shape[1] - 1] = 1
        dataset_classe2 = dataset[dataset[:, dataset.shape[1] - 1] == class2]
        dataset_classe2[:, dataset_classe1.shape[1] - 1] = -1

        return np.concatenate((dataset_classe1, dataset_classe2))