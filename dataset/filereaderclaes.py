import os


def read_file(file_path, file_name):
    file = open(
        os.path.normpath(os.getcwd() + os.sep + os.pardir + "/redes-neurais/Dataset/" + file_path + "/" + file_name),
        "r")
    dataset = file.readlines()
    file.close()
    return dataset
