import Dataset.datasetreader as dt
# import Dataset.filereader as fr
import dataset.filereaderclaes as fr
import Adaline.adaline as ada
import matplotlib.pyplot as plt
import Perceptron.perceptron as per
import numpy as np
import MultiLayerPerceptron.MLP
import Kohonen.SOMNeuron as neuron
import Kohonen.Kohonen
import Fuzzy.fuzzification as FuzzyClass


def exec_perceptron(numero_da_execucao):
    reader = dt.DatasetReader()
    perceptron = per.Perceptron()

    # faz a leitura do Dataset para as classes passadas
    dataset_training = reader.get_dataset_for_class(
        fr.read_file("Dataset#1", "xtrain.txt"),
        fr.read_file("Dataset#1", "dtrain.txt"),
        1,
        2
    )

    # faz o shuffle no Dataset
    dataset_training = np.take(dataset_training, np.random.permutation(dataset_training.shape[0]), axis=0)

    # separa do Dataset somente as entradas
    x_train = dataset_training[:, [i for i in range(dataset_training.shape[1] - 1)]]

    # separa do Dataset somente as saídas esperadas
    d_train = dataset_training[:, dataset_training.shape[1] - 1].reshape(dataset_training.shape[0], 1)

    # faz a leitura do Dataset para as classes passadas
    dataset_test = reader.get_dataset_for_class(
        fr.read_file("Dataset#3", "xtest.txt"),
        fr.read_file("Dataset#3", "dtest.txt"),
        1,
        2
    )

    # faz o shuffle no Dataset
    dataset_test = np.take(dataset_test, np.random.permutation(dataset_test.shape[0]), axis=0)

    # separa do Dataset somente as entradas
    x_test = dataset_test[:, [i for i in range(dataset_test.shape[1] - 1)]]

    # separa do Dataset somente as saídas esperadas
    d_test = dataset_test[:, dataset_test.shape[1] - 1].reshape(dataset_test.shape[0], 1)

    if numero_da_execucao == 0:
        perceptron.set_initial_weights(np.array([0, 0, 0]))
    else:
        np.random.seed(numero_da_execucao)
        perceptron.set_initial_weights(np.random.rand(len(x_train[0]) + 1))

    perceptron.set_training_mode(perceptron.CONST_OFFLINE)
    perceptron.set_entrys_outs(x_train, d_train, False, perceptron.CONST_TRAIN)
    perceptron.set_entrys_outs(x_test, d_test, False, perceptron.CONST_TEST)
    perceptron.training(learning_rate=1)
    perceptron.plot()
    perceptron.test()
    perceptron.plot()


def exec_adaline(numero_da_execucao):
    reader = dt.DatasetReader()
    adaline = ada.Adaline()

    # faz a leitura do Dataset para as classes passadas
    dataset_training = reader.get_dataset_for_class(
        fr.read_file("Dataset#3", "xtrain.txt"),
        fr.read_file("Dataset#3", "dtrain.txt"),
        1,
        -1
    )

    # faz o shuffle no Dataset
    dataset_training = np.take(dataset_training, np.random.permutation(dataset_training.shape[0]), axis=0)

    # separa do Dataset somente as entradas
    x_train = dataset_training[:, [i for i in range(dataset_training.shape[1] - 1)]]

    # separa do Dataset somente as saídas esperadas
    d_train = dataset_training[:, dataset_training.shape[1] - 1].reshape(dataset_training.shape[0], 1)

    # faz a leitura do Dataset para as classes passadas
    dataset_test = reader.get_dataset_for_class(
        fr.read_file("Dataset#3", "xtest.txt"),
        fr.read_file("Dataset#3", "dtest.txt"),
        1,
        -1
    )

    # faz o shuffle no Dataset
    dataset_test = np.take(dataset_test, np.random.permutation(dataset_test.shape[0]), axis=0)

    # separa do Dataset somente as entradas
    x_test = dataset_test[:, [i for i in range(dataset_test.shape[1] - 1)]]

    # separa do Dataset somente as saídas esperadas
    d_test = dataset_test[:, dataset_test.shape[1] - 1].reshape(dataset_test.shape[0], 1)

    if numero_da_execucao == 0:
        adaline.set_initial_weights(np.array([0, 0, 0, 0, 0]))
    else:
        np.random.seed(numero_da_execucao)
        adaline.set_initial_weights(np.random.rand(len(x_train[0]) + 1))

    adaline.set_training_mode(adaline.CONST_OFFLINE)
    adaline.set_entrys_outs(x_train, d_train, False, adaline.CONST_TRAIN)
    adaline.set_entrys_outs(x_test, d_test, False, adaline.CONST_TEST)
    adaline.train(0.0025, 0.00001)
    # adaline.plot()
    # adaline.train(0.025, 0.000006)
    adaline.plot_eqm_epoch()
    adaline.test()
    adaline.plot()


def exec_mlp(numero_da_execucao):
    reader = dt.DatasetReader()
    mlp = MultiLayerPerceptron.MLP.MLP()

    entrys = reader.get_dataset(fr.read_file("DatasetXnoise", "Xlarge.txt"))
    entry2 = reader.get_dataset(fr.read_file("DatasetXnoise", "Xnoise2.txt"))
    entry3 = reader.get_dataset(fr.read_file("DatasetXnoise", "Xnoise5.txt"))
    entry4 = reader.get_dataset(fr.read_file("DatasetXnoise", "Xnoise10.txt"))
    outs = reader.get_dataset(fr.read_file("DatasetXnoise", "Xlarge.txt"))

    mlp.setup(
        entrys=entrys,
        outs=outs,
        learning_rate=0.1,
        precision=0.001,
        momentum=0.5,
        neurons_in_hidden_layer=5,
        normalize=False,
        training_type=mlp.TRAINING_ONLINE
    )

    mlp.train()
    mlp.plot_eqm_epoch()

    mlp.test(entry2)

    mlp.test(entry3)

    mlp.test(entry4)


def exec_kohonen(num, qtd_neurons, qtd_dimens, learning_rate, standart_deviation, radius, alfa):
    reader = dt.DatasetReader()
    training_entrys = reader.get_dataset(fr.read_file("DatasetXnoise", "Xlarge.txt"))
    test_entrys = reader.get_dataset(fr.read_file("DatasetXnoise", "Xnoise10.txt"))
    entry_representation = ["A1", "B1", "C1", "D1", "E1", "J1", "K1",
                            "A2", "B2", "C2", "D2", "E2", "J2", "K2",
                            "A3", "B3", "C3", "D3", "E3", "J3", "K3"]

    kohonen = Kohonen.Kohonen.Kohonen()
    kohonen.setup(
        map_qtd_neurons=qtd_neurons,
        map_qtd_dimensions=qtd_dimens,
        precision=0.0001,
        learning_rate=learning_rate,
        standart_deviation=standart_deviation,
        neuron_radius=radius,
        training_entrys=training_entrys,
        test_entrys=test_entrys,
        entry_represention=entry_representation,
        alfa=alfa
    )
    print("EXECUCAO " + str(num + 1) + "=> QTD_NEURONIOS = " + str(qtd_neurons) + " -- DIMENSAO DO MAPA = " + str(
        qtd_dimens) + " -- TAXA DE APRENDIZADO = " + str(learning_rate) + " -- DESVIO PADRAO = " + str(
        standart_deviation) + " -- RAIO = " + str(radius) + " -- ALFA = " + str(alfa))
    kohonen.execute()


def exec_fuzzy():
    FuzzyClass.Fuzzy().setup()



if __name__ == "__main__":
    exec_fuzzy()
