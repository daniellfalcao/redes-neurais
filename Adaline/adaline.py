import numpy as np
import matplotlib.pyplot as plt
from Functions import functions as fun
import math


class Adaline:
    CONST_OFFLINE = "offline"
    CONST_ONLINE = "online"

    CONST_TRAIN = "train"
    CONST_TEST = "test"

    CONST_BIAS = -1

    def __init__(self):
        self.normalize = False
        self.execution_mode = self.CONST_TRAIN
        self.training_mode = self.CONST_ONLINE
        self.precision = 0
        self.learning_rate = 0
        self.epochs = 0
        self.eqm_previous = 1
        self.eqm_current = 10
        self.weights = np.empty([0])
        self.training_entrys = np.empty([0])
        self.test_entrys = np.empty([0])
        self.training_expected_outs = np.empty([0])
        self.test_expected_outs = np.empty([0])
        self.eqm_x_epoch = np.empty([0])
        self.training_scaler = None
        self.test_scaler = None
        self.epochs_array = np.empty([0])
        self.eqm_array = np.empty([0])

    def normalizacao(self, ents):
        soma = 0
        media = 0
        variancia = 0
        desv = 0

        matriz_normalize = np.zeros(shape=(ents.shape[0], ents.shape[1]))

        for collumn in range(ents.shape[1]):  # ents.shape[0] == linha ; ents.shape[0] == coluna
            soma = ents[:, collumn].sum()

        qtd_elementos = len(ents)

        media = soma / float(qtd_elementos)

        for collumn in range(ents.shape[1]):
            soma += np.sum(np.power(np.subtract(ents[:, collumn], media), 2))

        variancia = soma / (float(len(ents)) - 1)

        desv = math.sqrt(variancia)

        for line in range(ents.shape[0]):
            new_array = []
            for collumn in range(ents.shape[1]):
                new_array.append(
                    np.divide(
                        np.subtract(
                            ents.item((line, collumn))
                            , media)
                        , desv)
                )
            matriz_normalize[line] = new_array

        return matriz_normalize

    def set_entrys_outs(self, entrys, outs, normalize, execution_mode):
        self.normalize = normalize

        # normaliza
        if normalize:
            entrys = self.normalizacao(entrys)
            print("uuhhuu")

        # insere 1 na primeira coluna de todas as linhas da entrada
        entrys = np.insert(entrys, 0, [self.CONST_BIAS for column in entrys], axis=1)

        if execution_mode == self.CONST_TRAIN:
            self.training_expected_outs = outs
            self.training_entrys = entrys
        else:
            self.test_expected_outs = outs
            self.test_entrys = entrys

    def set_initial_weights(self, weights):
        self.weights = weights

    def set_training_mode(self, mode):
        self.training_mode = mode

    def train(self, learning_rate, precision):

        self.execution_mode = self.CONST_TRAIN
        self.learning_rate = learning_rate
        self.precision = precision

        self.epochs = 0
        right = 0
        u = np.empty([0])
        while self.epochs <= 10000 and not self.precision >= abs(self.eqm_current - self.eqm_previous):
            self.eqm_previous = self.calc_eqm(u)
            u = np.empty([0])
            right = 0
            for row in range(self.training_entrys.shape[0]):
                u = self.concate_to_array(u, self.calc_y(row))
                if self.training_mode == self.CONST_ONLINE:
                    self.weights = self.calc_weights_online(u[row], row)
                else:
                    right += 1                                                              # TROCAR OA TAXA DE ACERTO NO TRINEMANTO, ESTA ERRADO O COUNT DE ACERTOS
            if self.training_mode == self.CONST_OFFLINE:
                self.weights = self.calc_weights_offline(u)
            self.epochs += 1
            self.eqm_current = self.calc_eqm(u)
            self.eqm_array = np.concatenate([self.eqm_array, np.array([self.eqm_current])])
            self.epochs_array = np.concatenate([self.epochs_array, np.array([self.epochs])])
            print("epoca = %d" % self.epochs + "erro = %s" % str(abs(self.eqm_current - self.eqm_previous)))
        print("Quantidade de épocas = %d" % self.epochs)
        print("Os valores dos pesos são: ", self.weights)
        print("Taxa de Acerto no treinamento = ", (right / self.training_entrys.shape[0]), "%")

    def test(self):

        self.execution_mode = self.CONST_TEST
        hits_classe1 = 0
        mistakes_classe1 = 0
        hits_classe2 = 0
        mistakes_classe2 = 0

        y = np.empty([0])
        for row in range(self.test_entrys.shape[0]):
            y = self.concate_to_array(y, self.calc_y(row))
            if y[row] == self.test_expected_outs[row]:
                if self.test_expected_outs[row] == 1:
                    hits_classe1 += 1
                else:
                    hits_classe2 += 1
            else:
                if self.test_expected_outs[row] == 1:
                    mistakes_classe1 += 1
                else:
                    mistakes_classe2 += 1

        print("ACERTOS DA CLASSE 1: %d" % hits_classe1)
        print("ERROS DA CLASSE 1: %d" % mistakes_classe1)
        print("ACERTOS DA CLASSE 2: %d" % hits_classe2)
        print("ERROS DA CLASSE 2: %d" % mistakes_classe2)

    def calc_eqm(self, u):

        """ Função de cálculo do Eqm(w) = (1 / p) * ((pΣk=0)(d(k) * u) ** 2) """

        if self.execution_mode == self.CONST_TRAIN:
            entrys = self.training_entrys
            outs = self.training_expected_outs
        else:
            entrys = self.test_entrys
            outs = self.test_expected_outs

        if u.shape[0] == 0:
            return 0

        summation = 0
        for k in range(entrys.shape[0]):
            summation += (outs[k][0] - u[k][0]) ** 2

        return (1 / entrys.shape[0]) * summation
        pass

    def calc_y(self, row):

        """ Função de cálculo do g(u).

            <1> u = ((nΣi=0) wi * xi)
            <2> durante o treinamento g(u) = u
            <3> durante o teste g(u) = 1, se u >= 0 | -1, se u < 0

        """

        if self.execution_mode == self.CONST_TRAIN:
            entrys = self.training_entrys
        else:
            entrys = self.test_entrys

        u = self.weights.dot(entrys[row])

        if self.execution_mode == self.CONST_TRAIN:
            print("valor esperado = ", (self.training_expected_outs[row][0]))
            print("valor encontrado = ", u)
            print("erro = ", (self.training_expected_outs[row][0] - u))
            return u
        else:
            return fun.step_function(u)

    def calc_weights_online(self, u, row):

        if self.execution_mode == self.CONST_TRAIN:
            entrys = self.training_entrys
            outs = self.training_expected_outs
        else:
            entrys = self.test_entrys
            outs = self.test_expected_outs

        return self.weights + (self.learning_rate * (outs[row][0] - u) * entrys[row])

    def calc_weights_offline(self, u):

        """ Função de cálculo dos novos pesos

            Considerando o treinamento offline temos:
                w = w + (n/p) * ((pΣk=0) (d(k) - u) * x(k)

            Considerando o treinamento online temos:
                w = w + n * (d(k) - u) * x(k), onde k = 1, ..., p
        """

        if self.execution_mode == self.CONST_TRAIN:
            entrys = self.training_entrys
            outs = self.training_expected_outs
        else:
            entrys = self.test_entrys
            outs = self.test_expected_outs

        summation = 0
        for k in range(entrys.shape[0]):
            summation += (outs[k][0] - u[k][0]) * entrys[k]
        return self.weights + ((self.learning_rate / entrys.shape[0]) * summation)

    def plot_eqm_epoch(self):
        plt.tight_layout()
        plt.plot(self.epochs_array, self.eqm_array, '-r', color='k')
        plt.title('EQM x ÉPOCA')
        plt.xlabel('ÉPOCA', color='#000000')
        plt.ylabel('EQM', color='#000000')
        plt.grid()
        plt.show()

    def plot(self):

        if self.execution_mode == self.CONST_TRAIN:
            entrys = self.training_entrys
            outs = self.training_expected_outs
        else:
            entrys = self.test_entrys
            outs = self.test_expected_outs

        classe1 = entrys[outs[:, 0] > 0]
        classe2 = entrys[outs[:, 0] < 0]

        x = entrys[:, [i for i in range(1, entrys.shape[1])]]

        max_x = x[:, 0].max() + 1
        min_x = x[:, 0].min() - 1

        classe1 = entrys[outs[:, 0] > 0]
        classe2 = entrys[outs[:, 0] < 0]

        plt.tight_layout()
        x = np.linspace(min_x, max_x)
        y = (-self.weights[1] * x + self.weights[0]) / self.weights[2]

        plt.plot(x, y, '-r', color='k')
        plt.title('Adaline')
        plt.xlabel('eixo x', color='#000000')
        plt.ylabel('eixo y', color='#000000')
        plt.scatter(classe1[:, 1], classe1[:, 2], color='r', label='Classe1')
        plt.scatter(classe2[:, 1], classe2[:, 2], color='b', label='Classe2')
        plt.grid()
        plt.show()

    @staticmethod
    def concate_to_array(array1, value):
        try:
            return np.vstack((array1, [value]))
        except:
            return np.array([value])
