import matplotlib.pyplot as plt
import numpy as np
from sklearn import preprocessing

from Functions import functions as fun


class Perceptron:
    CONST_OFFLINE = "offline"
    CONST_ONLINE = "online"

    CONST_TRAIN = "train"
    CONST_TEST = "test"

    CONST_BIAS = -1

    def __init__(self):
        self.normalize = False
        self.learning_rate = 1
        self.execution_mode = self.CONST_TRAIN
        self.training_mode = self.CONST_ONLINE
        self.epochs = 0
        self.weights = np.empty([0])
        self.training_entrys = np.empty([0])
        self.test_entrys = np.empty([0])
        self.training_expected_outs = np.empty([0])
        self.test_expected_outs = np.empty([0])
        self.training_scaler = None
        self.test_scaler = None

    def set_entrys_outs(self, entrys, outs, normalize, execution_mode):
        self.normalize = normalize

        # insere 1 na primeira coluna de todas as linhas da entrada
        entrys = np.insert(entrys, 0, [-1 for column in entrys], axis=1)

        if execution_mode == self.CONST_TRAIN:
            self.training_expected_outs = outs
            if normalize:
                self.training_scaler = preprocessing.MinMaxScaler(feature_range=(-1, 1)).fit(entrys)
                self.training_entrys = self.training_scaler.transform(entrys)
            else:
                self.training_entrys = entrys
        else:
            self.test_expected_outs = outs
            if normalize:
                self.test_scaler = preprocessing.MinMaxScaler(feature_range=(-1, 1)).fit(entrys)
                self.test_entrys = self.test_scaler.transform(entrys)
            else:
                self.test_entrys = entrys

    def set_initial_weights(self, weights):
        self.weights = weights

    def set_training_mode(self, mode):
        self.training_mode = mode

    def training(self, learning_rate=1):

        self.execution_mode = self.CONST_TRAIN
        self.learning_rate = learning_rate
        self.epochs = 0

        has_error = True

        while has_error and self.epochs <= 10000:
            y = np.empty([0])
            has_error = False
            for row in range(self.training_entrys.shape[0]):
                y = self.concate_to_array(y, self.calc_y(row))
                if y[row] != self.training_expected_outs[row]:
                    has_error = True
                    if self.training_mode == self.CONST_ONLINE:
                        self.weights = self.calc_weights_online(y[row], row)
            if self.training_mode == self.CONST_OFFLINE:
                self.weights = self.calc_weights_offline(y)
            self.epochs += 1

    def test(self):

        self.execution_mode = self.CONST_TEST
        hits_classe1 = 0
        mistakes_classe1 = 0
        hits_classe2 = 0
        mistakes_classe2 = 0

        y = np.empty([0])
        for row in range(self.test_entrys.shape[0]):
            y = self.concate_to_array(y, self.calc_y(row))
            if y[row] == self.test_expected_outs[row]:
                if self.test_expected_outs[row] == 1:
                    hits_classe1 += 1
                else:
                    hits_classe2 += 1
            else:
                if self.test_expected_outs[row] == 1:
                    mistakes_classe1 += 1
                else:
                    mistakes_classe2 += 1

        print("ACERTOS DA CLASSE 1: %d" % hits_classe1)
        print("ERROS DA CLASSE 1: %d" % mistakes_classe1)
        print("ACERTOS DA CLASSE 2: %d" % hits_classe2)
        print("ERROS DA CLASSE 2: %d" % mistakes_classe2)
        print("Os pesos são: ", self.weights)
        print("Numero de épocas: ", self.epochs)

    def calc_y(self, row):

        """ Função de cálculo do g(u).

            <1> u = ((nΣi=0) wi * xi)
            <2> durante o treinamento g(u) = u
            <3> durante o teste g(u) = 1, se u >= 0 | -1, se u < 0

        """

        entrys = self.get_entrys()

        u = self.weights.dot(entrys[row])
        return fun.step_function(u)

    def calc_weights_online(self, u, row):

        entrys = self.get_entrys()
        outs = self.get_expected_outs()

        return self.weights + (self.learning_rate * (outs[row][0] - u) * entrys[row])

    def calc_weights_offline(self, y):

        """ Função de cálculo dos novos pesos w = w + (n/p) * ((pΣk=0) (d(k) - u) * x(k) """

        entrys = self.get_entrys()
        outs = self.get_expected_outs()

        summation = 0
        for k in range(entrys.shape[0]):
            summation += (outs[k][0] - y[k][0]) * entrys[k]
        return self.weights + ((self.learning_rate / entrys.shape[0]) * summation)

    def get_entrys(self):
        if self.execution_mode == self.CONST_TRAIN:
            return self.training_entrys
        else:
            return self.test_entrys

    def get_expected_outs(self):
        if self.execution_mode == self.CONST_TRAIN:
            return self.training_expected_outs
        else:
            return self.test_expected_outs

    def plot(self):

        if self.execution_mode == self.CONST_TRAIN:
            plt.title('Perceptron - Treino')
            entrys = self.training_entrys
            outs = self.training_expected_outs
        else:
            plt.title('Perceptron - Teste')
            entrys = self.test_entrys
            outs = self.test_expected_outs

        x = entrys[:, [i for i in range(1, entrys.shape[1])]]

        max_x = x[:, 0].max() + 1
        min_x = x[:, 0].min() - 1

        classe1 = entrys[outs[:, 0] > 0]
        classe2 = entrys[outs[:, 0] < 0]

        plt.tight_layout()
        x = np.linspace(min_x, max_x)
        y = (-self.weights[1] * x + self.weights[0]) / self.weights[2]

        plt.plot(x, y, '-r', color='k')
        plt.xlabel('eixo x', color='#000000')
        plt.ylabel('eixo y', color='#000000')
        plt.scatter(classe1[:, 1], classe1[:, 2], color='r', label='Classe1')
        plt.scatter(classe2[:, 1], classe2[:, 2], color='b', label='Classe2')
        plt.grid()
        plt.show()

    @staticmethod
    def concate_to_array(array1, value):
        try:
            return np.vstack((array1, [value]))
        except:
            return np.array([value])
